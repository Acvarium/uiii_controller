#include <Arduino.h>
#include <Joystick.h>
#include <MPU6050_tockn.h>
#include <Wire.h>
#include "Keyboard.h"
#include <EEPROM.h>

MPU6050 mpu6050(Wire);
int angle_range = 160;
const int jxAxis = A0;         // joystick X axis
const int jyAxis = A1;         // joystick Y axis
const int vib = 12;
const int jButton = 11;
bool button0 = false;
int jx;
int jy;

char menu_combo[] = {1,-1, 10, -10};
byte combo_n = 0;

#define LINE_SIZE 21
byte sel = 0;
byte multy_menu_id = 0;
byte profile_id = 0;

String main_menu_elements[LINE_SIZE] = {
        "Current Profile: #",
        "Edit Profile",
        "List Profile Values",
        "Exit"
};

String edit_profile_elements[LINE_SIZE] = {
        "Back",
        "Gyro Angle Range: ",
        "Buttons",
};

byte menu_sizes[] = {4,3};

char direction = 0;

unsigned long menu_press_time = 0;
const unsigned int max_press_delay = 3000;
bool menu_on = false;
byte eee;
byte rows[] = {7,8,9,10};
const int rowCount = sizeof(rows)/sizeof(rows[0]);
byte cols[] = {4,5,6};
const int colCount = sizeof(cols)/sizeof(cols[0]);

byte keys[colCount * rowCount];
byte keys_states[colCount * rowCount];

Joystick_ Joystick(JOYSTICK_DEFAULT_REPORT_ID,JOYSTICK_TYPE_GAMEPAD,
                   13, 0, // Button Count, Hat Switch Count
                   true, true, false, // X Y, no Z
                   true, true, true, // Rx, Ry, Rz
                   false, false, // No rudder or throttle
                   false, false, false); // No accelerator, brake, or steering

void vibrate(int _millis){
        digitalWrite(vib, HIGH);
        delay(_millis);
        digitalWrite(vib, LOW);
}

void list_menu(){
        Keyboard.println();
        for (int i = 0; i < menu_sizes[multy_menu_id]; i++) {
                if (sel == i) {
                        Keyboard.print("==>  ");
                }
                if (multy_menu_id == 0) {
                        Keyboard.print(main_menu_elements[i]);
                        if (i == 0) {
                                Keyboard.print(String(profile_id));
                        }
                }
                else if (multy_menu_id == 1) {
                        Keyboard.print(edit_profile_elements[i]);
                        if (i == 1) {
                                Keyboard.print(String(angle_range));
                        }
                }

                Keyboard.println();
        }
}

void apply_profile(){
  eee = EEPROM.read(1 + (profile_id * 56));
  if (eee >= 20 && eee <= 180){
    angle_range = eee;
  }
  else{
    angle_range = 60;
    EEPROM.write(1 + (profile_id * 56), angle_range);
  }
  Joystick.setRxAxisRange(-angle_range, angle_range);
  Joystick.setRyAxisRange(-angle_range, angle_range);
  Joystick.setRzAxisRange(-angle_range, angle_range);
}

void setup() {
        Serial.begin(9600);
        Serial.print("menu_sizes ");
        Serial.println(menu_sizes[0]);
        Keyboard.begin();
        pinMode(vib, OUTPUT);
        for(int x=0; x<rowCount; x++) {
                Serial.print(rows[x]); Serial.println(" as input");
                pinMode(rows[x], INPUT);
        }
        for (int x=0; x<colCount; x++) {
                Serial.print(cols[x]); Serial.println(" as input-pullup");
                pinMode(cols[x], INPUT_PULLUP);
        }

        pinMode(jButton, INPUT);
        vibrate(150);
        profile_id = EEPROM.read(0);
        if(profile_id > 9) {
                profile_id = 0;
                EEPROM.write(0, 0);
        }

        Wire.begin();
        mpu6050.begin();
        mpu6050.calcGyroOffsets(true);

        Joystick.begin(false);
        Joystick.setXAxisRange(5, 1016);
        Joystick.setYAxisRange(5, 1016);

        apply_profile();
        vibrate(200);
        delay(200);
        vibrate(400);
}

void press_key(int n, bool _press){
        Joystick.setButton(n+1, _press);
}

void menu(){
        menu_on = !menu_on;
        if(menu_on) {
                Serial.println("-- Enter Menu --");
                Keyboard.print("\n-- Enter Menu --\n");
                list_menu();
                vibrate(200);
                delay(200);
                vibrate(400);
                delay(100);
                vibrate(600);
        }
        else{
                Serial.println("++ Exit Menu ++");
                Keyboard.print("\n++ Exit Menu ++\n");
                sel = 0;
                vibrate(600);
                delay(100);
                vibrate(200);
                delay(100);
                vibrate(200);
        }
}

void edit_element(int value){
        if (multy_menu_id == 0) {
                if (sel == 0 && value != 0) {
                        profile_id += value;
                        if (profile_id > 9) { profile_id = 0; }
                        EEPROM.write(0, (byte)profile_id);
                        list_menu();
                        apply_profile();
                        vibrate(100);
                }
                else if (sel == 3) {
                        menu();
                }
                else if (sel == 1) {
                        multy_menu_id = 1;
                        sel = 0;
                        list_menu();
                        vibrate(100);
                }
        }
        else if (multy_menu_id == 1) {
                if (sel == 0) {
                        multy_menu_id = 0;
                        sel = 0;

                        list_menu();
                        vibrate(100);
                }
                else if (sel == 1 && value != 0){
                  angle_range += value * 10;
                  if (angle_range > 180){ angle_range = 180; }
                  if (angle_range < 20){ angle_range = 20; }
                  EEPROM.write(1 + (profile_id * 56), (byte)angle_range);
                  Joystick.setRxAxisRange(-angle_range, angle_range);
                  Joystick.setRyAxisRange(-angle_range, angle_range);
                  Joystick.setRzAxisRange(-angle_range, angle_range);
                  list_menu();
                  vibrate(100);
                }
        }
}

void readMatrix() {
        // iterate the columns
        int n = 0;
        for (int colIndex=0; colIndex < colCount; colIndex++) {
                byte curCol = cols[colIndex];
                pinMode(curCol, OUTPUT);
                digitalWrite(curCol, LOW);

                for (int rowIndex=0; rowIndex < rowCount; rowIndex++) {
                        byte rowCol = rows[rowIndex];
                        pinMode(rowCol, INPUT_PULLUP);
                        keys[n] = !digitalRead(rowCol);
                        if (keys[n] != keys_states[n]) {
                                press_key(n, keys[n]);
                                combo_n = 0;
                        }

                        keys_states[n] = keys[n];
                        pinMode(rowCol, INPUT);
                        n++;
                }
                pinMode(curCol, INPUT);
        }
}

void loop() {
        mpu6050.update();
        int analogX = analogRead(jxAxis);
        int analogY = analogRead(jyAxis);
        bool _button0 = !digitalRead(jButton);
        char d = 0;
        if (analogX > 1010) {
                d = 1;
        }
        else if (analogX < 10) {
                d = -1;
        }
        if (analogY > 1010) {
                d -= 10;
        }
        else if (analogY < 10) {
                d += 10;
        }

        if (direction != d && d != 0) {
                if(d == menu_combo[combo_n]) {
                        combo_n++;
                        vibrate((int)combo_n * 30);
                        if (combo_n > 4) {
                                combo_n = 0;
                        }
                }
                else{
                        combo_n = 0;
                }
                Serial.println(combo_n);
        }
        if (_button0 && !button0 && combo_n == 4) {
                combo_n = 0;
                menu();
        }

        if (menu_on) {
                if (direction != d && d != 0) {
                        if(d == -1 && sel < 3) {
                                sel++;
                                list_menu();
                        }
                        else if (d == 1 && sel > 0) {
                                sel--;
                                list_menu();
                        }
                        else if (d == 10) {
                                edit_element(1);
                        }
                        else if (d == -10) {
                                edit_element(-1);
                        }
                }
                if (_button0 && !button0) {
                        edit_element(0);
                }

        }
        direction = d;

        jx = analogX;
        jy = analogY;
        readMatrix();

        button0 = _button0;
        if (!menu_on) {
          Joystick.setXAxis(jx);
          Joystick.setYAxis(jy);

          Joystick.setRxAxis(mpu6050.getAngleX());
          Joystick.setRyAxis(mpu6050.getAngleY());
          Joystick.setRzAxis(mpu6050.getAngleZ());
          Joystick.setButton(0, _button0);
          Joystick.sendState();
        }
        delay(30);

}
